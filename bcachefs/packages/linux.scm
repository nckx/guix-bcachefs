;;; nckx's packages --- More functional packages for GNU
;;; Copyright © 2020-2022 Tobias Geerinckx-Rice <me@tobias.gr>
;;;
;;; This program is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(define-module (bcachefs packages linux)
  #:use-module (gnu packages linux)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define linux-libre-bcachefs-revision "0")
(define linux-libre-bcachefs-commit "cea583fa17b51f316f68ce46da1a380d7c28ce0c")
(define linux-libre-bcachefs-base-version "5.15")
(define linux-libre-bcachefs-gnu-revision "gnu")

(define deblob-scripts-bcachefs
  ((@@ (gnu packages linux) linux-libre-deblob-scripts)
   linux-libre-bcachefs-base-version
   linux-libre-bcachefs-gnu-revision
   (base32 "1rfhwfzifmbpnrhmrn3srm736nkm1v6affw915d0fgqzqgi8qfai")
   (base32 "162cpvxw2hfvmcwds3n39gw8m1qydryrvmvvff97db07rzq3i8mh")))

(define linux-libre-bcachefs-version
  ;; XXX Using GIT-VERSION here would confuse VERSION-MAJOR+MAJOR and cause a
  ;; bogus fatal error.
  (string-append linux-libre-bcachefs-base-version ".guix"
                 linux-libre-bcachefs-revision "-"
                 linux-libre-bcachefs-commit))

(define (%bcachefs-linux-source commit hash)
  (origin
   (method git-fetch)
   (uri (git-reference
	 (url "https://github.com/koverstreet/bcachefs")
	 (commit commit)))
   (sha256 hash)))

(define-public linux-libre-bcachefs-pristine-source
  (let ((commit linux-libre-bcachefs-commit)
        (hash (base32 "0msigy69rz22ikl75kng0p05kpb4whhrz4672p6whrsx678jbmyf")))
    ((@@ (gnu packages linux) make-linux-libre-source)
     linux-libre-bcachefs-version
     (%bcachefs-linux-source commit hash)
     deblob-scripts-bcachefs)))

;; Nitpick: see its description for why this is not named linux-libre-with-bcachefs.
(define-public linux-libre-bcachefs
  (package
   (inherit ((@@ (gnu packages linux) make-linux-libre*)
	     linux-libre-bcachefs-version
             linux-libre-bcachefs-gnu-revision
             linux-libre-bcachefs-pristine-source
             '("x86_64-linux")
             #:extra-version "bcachefs"
             #:configuration-file (@@ (gnu packages linux) kernel-config)
             #:extra-options
             (append `(("CONFIG_BCACHEFS_FS" . m))
		     (@@ (gnu packages linux) %default-extra-linux-options))))
    (synopsis "100% free bcachefs development fork of the Linux kernel")
    (description
     "This package is a free (as in freedom) variant of the Linux kernel used
to develop bcachefs, a new copy-on-write file system for Linux.  It has been
modified to remove all non-free binary blobs.

WARNING: this package may contain known unpatched security vulnerabilities.
It tracks the bcachefs upstream repository, which focuses on file system
development and updates the underlying kernel only occasionally.")))

linux-libre-bcachefs
